Other Math
##########

degreesToRadians(``degrees``)
*****************************
**Description**

Converts degrees to radians.

**Parameters**

=========== ======
``degrees`` Number
=========== ======

**Type**

Number

----

radiansToDegrees(``radians``)
*****************************
**Description**

Converts radians to degrees.

**Parameters**

=========== ======
``radians`` Number
=========== ======

**Type**

Number
